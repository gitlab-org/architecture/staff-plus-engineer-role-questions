# How do you handle "growing into the role"?

**Manuel Grabowski, Senior Support Engineer**: At GitLab the general philosophy
is that you're promoted when you're already performing at the new level anyway.
One thing I find challenging with this approach: Performing at the Senior level
as an Intermediate, or at the Staff level as a Senior etc., means that I don't
have as much time available to perform at the current level tasks as I used to.
Of course you're also becoming more efficient over time, but at least in my
personal experience that can't (and isn't expected to) fully outweigh this
effect.

For me, that means my imposter syndrome sometimes has a field day – because
when I'm performing "worse" at my Senior responsibilities due to focussing on
things I consider Staff level, how could I ever dare to think I'm good enough
for Staff. I'm curious if this is something you experienced as well and if you
have any tips/strategies on how to approach this.

**Cynthia "Arty" Ng, Staff Support Engineer**: [...] from a Support perspective [...]:

I think this is where a development plan is important, and regularly
communicating with your manager comes in. One part of this is discussing what's
"good enough" to be considered for a promotion ("Performing" in 2 areas, and
"Developing" in the rest, or the reverse?
[the Staff Support Engineer performance worksheet](https://drive.google.com/drive/search?q=Support%20Engineering%20-%20Performance%20Factor%20Template)
is still being iterated on but made available to all Support team members for
viewing recently). Since your manager is the one doing your performance review
and evaluating your work, it's important to discuss how much Staff-level work
(that doesn't overlap with Senior responsibilities) you're taking on, and what
the expectations are for the Senior-level work.

To give you a concrete example, when working towards Staff, I spent more time
doing "project" work and GitLab product MRs. As I still spent a lot of time
helping people, the thing that took a hit was the number of tickets I picked up
(FRT) and was assigned to. While I was still meeting the "baseline", it was not
the same level of "performance" I previously had, so my manager and I agreed to
look at 2 additional ticket metrics: public comments, and internal comments.
Public comments were still expected to go down a little, but internal comments
should not decrease. We also added number and weight of product MRs as
something to track. While we didn't do it, number of assigned/resolved team
(meta) issues could be something to track as well.

One of the key things in _what_ to spend that time on is something I recommend
discussing with your skip-level leader (director in Support's case). Because a
Staff engineer is supposed to fulfill a "business need", you need to understand
what that need is and focus on work that exemplifies that. I think this is
doubly important in Support where the scope of work is so broad, it's difficult
as an IC or even manager to identify what Senior leadership considers a
strategic-level business need. (I personally had a different idea of what
business need I could fulfill from my then-director.)

Though you're likely aware of them already, we do have a couple of resources
specifically within Support around getting promoted to Staff and what the Staff
role looks like:

1. [Path to Promotion: Staff Support Engineer](https://about.gitlab.com/handbook/support/support-engineer-career-path.html#path-to-promotion-staff-engineering)
   including a link to the AMA we did.
2. [Staff Support Engineer page](https://about.gitlab.com/handbook/support/engineering/staff.html)

**Grzegorz Bizon, Principal Engineer**: Excellent question! I can only offer
here my own perspective here, and it may differ from how other people think
about "growing into the role". For me, personally, there has always been just
one role: an Engineer. A single role that you are growing within.

In 2015, when I joined GitLab, all engineers were just "Developers". All
"Developers" were reporting to a Chief Operating Officer who had had around 10
people reporting to them at that time. 10 people with industry experience
varying between 1 and 10+ years. Then we introduced "Senior Developer" level
and started introducing more "Engineering" roles later, as the company grew. My
point here is that roles, titles and expectations for them are always relative
to the company size and business need. These are not necessarily consistent
across the industry too. My belief is that instead of focusing on your "next
level" it might be better to **focus on your growth** as an engineer in
general, by finding ways to improve how you and your colleagues work,
broadening your positive impact and eventually getting solid Results through
exemplifying company values.

By focusing on your _growth_, rather than the _next level_, the content in
Handbook about responsibilities of a Staff (Something) Engineer becomes just a
guidance for you; a few helpful tips to better understand what company expects
from senior technical leaders at this time. This can change. It most likely
will. And that is fine!

It is always a good idea to discuss these current expectations, for your
current and next role, with your manager, to make sure that you are aligned.
And then it is up to you to find ways to keep growing as a professional.
Continuous growth is part of the job here, and you need to be deliberate about
it, otherwise you risk your skills getting obsolete when the world moves
forward. You can use company-provided guidance around what are the expectations
for the "next level", you can read amazing books about the Staff+ Engineer
journey, or ask a Principal Engineer a few great questions. But ultimately you
will need to find your own way. Everyone is different.

How do I know if I'm growing as a professional? I think that, among other
things, it is about not feeling comfortable sometimes. Ray Dalio in his
[Principles](https://www.amazon.com/Principles-Life-Work-Ray-Dalio/dp/1501124021)
says that you should appreciate a "good struggle". It seems that your
challenges to balance your responsibilities at the current role with what the
company might expect from you in your "next role", is exactly this kind of a
"good struggle". You are figuring things out. It should be appreciated. We
sometimes try to make two steps forward, see challenges and some pushback, make
one step back, and then one forward again. Eventually we are growing. If you
are deliberate about pursuing growth, suddenly you will discover yourself
operating within the expectations for a Staff Engineer. It usually comes as a
surprise when you realize that.

## References

Source: https://gitlab.com/gitlab-org/architecture/staff-plus-engineer-role-questions/-/issues/1
