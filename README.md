# Staff+ Engineer Role Questions

The purpose of this project is to build an informal knowledge-base around "What
does it mean to be a Staff+ Engineer?" at GitLab and beyond.

It got inspired by a bookclub organized at GitLab around reading the excellent
[The Staff Engineer's Path](https://www.oreilly.com/library/view/the-staff-engineers/9781098118723/) book.

## Questions and answers

1. [How do you handle "growing into the role"?](growing_into_the_role.md)

## Contributing

1. Ask a question somewhere! Create an issue in this project or reach out on
   Slack to `@grzegorz`. Grzegorz will create an issue on your behalf without
   mentioning who asked it.
1. Answer the question in an issue if you have a good answer you would like to
   share.
1. Contribute your answer in a merge request if a file with the question
   already exists.

## License

[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
